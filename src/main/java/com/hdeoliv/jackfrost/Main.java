package com.hdeoliv.jackfrost;

import io.github.cdimascio.dotenv.Dotenv;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

public class Main {

    public static void main(String[] args) {
        final Dotenv dotenv = Dotenv.load();
        final String token = dotenv.get("TOKEN");
        DiscordApi api = new DiscordApiBuilder()
                .setToken(token)
                .login()
                .join();
        runCommands(api);
        System.out.println("You can invite the bot by using the following url: " + api.createBotInvite());
    }

    static void runCommands(DiscordApi api) {
        CommandsBasic.Ping(api);
        CommandsBasic.HighFive(api);

        CommandsMath.Sqrt(api);
        CommandsMath.Power(api);
        CommandsMath.Cos(api);
        CommandsMath.Sin(api);
        CommandsMath.Tan(api);
    }
}

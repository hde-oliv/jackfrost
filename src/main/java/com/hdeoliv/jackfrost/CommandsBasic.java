package com.hdeoliv.jackfrost;

import org.javacord.api.DiscordApi;

public class CommandsBasic implements Characteristics{
    public static void Ping(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(prefix + "ping")) {
                try {
                    event.getChannel().sendMessage("Pong!");
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }

    public static void HighFive(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(prefix + "hi")) {
                try {
                    event.getChannel().sendMessage("Five!");
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }
}

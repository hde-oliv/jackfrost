package com.hdeoliv.jackfrost;

import org.javacord.api.DiscordApi;

import java.util.Arrays;
import java.util.List;

public class CommandsMath implements Characteristics {
    public static void Sqrt(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().startsWith(prefix + "sqrt")) {
                List<String> args = Arrays.asList(event.getMessageContent().split(" "));
                try {
                    double resultValue = Math.sqrt(Double.parseDouble(args.get(1)));
                    event.getChannel().sendMessage(Double.toString(resultValue));
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }

    public static void Power(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().startsWith(prefix + "power")) {
                List<String> args = Arrays.asList(event.getMessageContent().split(" "));
                try {
                    double resultValue = Math.pow(Double.parseDouble(args.get(1)), Double.parseDouble(args.get(2)));
                    event.getChannel().sendMessage(Double.toString(resultValue));
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }

    public static void Cos(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().startsWith(prefix + "cos")) {
                List<String> args = Arrays.asList(event.getMessageContent().split(" "));
                try {
                    double resultValue = Math.cos(Double.parseDouble(args.get(1)));
                    event.getChannel().sendMessage(Double.toString(resultValue));
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }

    public static void Sin(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().startsWith(prefix + "sin")) {
                List<String> args = Arrays.asList(event.getMessageContent().split(" "));
                try {
                    double resultValue = Math.sin(Double.parseDouble(args.get(1)));
                    event.getChannel().sendMessage(Double.toString(resultValue));
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }

    public static void Tan(DiscordApi api) {
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().startsWith(prefix + "tan")) {
                List<String> args = Arrays.asList(event.getMessageContent().split(" "));
                try {
                    double resultValue = Math.tan(Double.parseDouble(args.get(1)));
                    event.getChannel().sendMessage(Double.toString(resultValue));
                } catch (Exception e) {
                    event.getChannel().sendMessage(errorMessage + e);
                }
            }
        });
    }
}
